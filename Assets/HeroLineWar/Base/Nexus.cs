using HeroLineWar.Creatures;
using UnityEngine;

namespace HeroLineWar.Base
{
    public class Nexus : MonoBehaviour, IAttackable
    {
        #region Variables

        public LayerMask mMonsterLayer;
        public int mMaxHealthPoints = 5000;
        public int mCurrentHealthPoints;
        public float mAttackRange = 1f;
        public float mAttackCooldown = 1f;

        private float _mNextAttackTimestamp = 0;

        #endregion
        
        #region Built-in methods

        // Start is called before the first frame update
        void Start()
        {
            mCurrentHealthPoints = mMaxHealthPoints;
        }

        // Update is called once per frame
        void Update()
        {
            // Vérification du cooldown avant d'attaquer
            float currentTime = Time.time;
            if (currentTime >= _mNextAttackTimestamp)
            {
                AttackAllMonstersInRange();
            }
        }

        #endregion

        #region Implemented methods

        public void Attack(IAttackable target)
        {
            int damage = (int)(target.GetMaxHealthPoints() * 0.50f);
            target.TakeDamage(damage);
        }

        public void TakeDamage(int damageAmout)
        {
            mCurrentHealthPoints -= damageAmout;
        }

        public int GetCurrentHealthPoints()
        {
            return mCurrentHealthPoints;
        }

        public int GetMaxHealthPoints()
        {
            return mMaxHealthPoints;
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public bool IsTargetable()
        {
            return true;
        }

        #endregion

        #region Custom methods

        void AttackAllMonstersInRange()
        {
            // Récupération des entités à portée d'attaque
            Collider[] colliders = Physics.OverlapSphere(transform.position, mAttackRange, mMonsterLayer);
            foreach (var collider in colliders)
            {
                // On attaque les Monster
                Monster currentMonster = collider.GetComponent<Monster>();
                if (currentMonster)
                {
                    Attack(currentMonster);
                }
            }
            // Mise à jour du cooldown
            float currentTime = Time.time;
            _mNextAttackTimestamp = currentTime + mAttackCooldown;
        }

        #endregion
    }
}
