using UnityEngine;

namespace HeroLineWar.Base
{
    public interface IAttackable
    {
        public void Attack(IAttackable target);
        public void TakeDamage(int damageAmout);
        public int GetCurrentHealthPoints();
        public int GetMaxHealthPoints();
        public Vector3 GetPosition();
        public bool IsTargetable();
    }
}