using HeroLineWar.Creatures;
using UnityEngine;

namespace HeroLineWar.Base
{
    [RequireComponent(typeof(SphereCollider))]
    public class HeroDetector : MonoBehaviour
    {
        public Hero mDetectedHero;
        public SphereCollider mDetectionArea;
        
        
        // Start is called before the first frame update
        void Start()
        {
            mDetectionArea = GetComponent<SphereCollider>();
        }

        // Update is called once per frame
        void Update()
        {
        
        }
        
        private void OnTriggerEnter(Collider other)
        {
            Hero detectedHero = other.GetComponent<Hero>();
            if (detectedHero)
            {
                mDetectedHero = detectedHero;
            }
        }
        
        private void OnTriggerExit(Collider other)
        {
            mDetectedHero = null;
        }
    }
}
