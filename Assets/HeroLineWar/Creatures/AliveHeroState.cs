using HeroLineWar.Base;
using HeroLineWar.Managers;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

namespace HeroLineWar.Creatures
{
    public class AliveHeroState : IHeroState
    {
        #region Variables

        private Hero _mHero;

        #endregion

        #region Implemented methods

        public void InitState(Hero hero)
        {
            HeroManager heroManager = GameObject.Find("GameManager").GetComponent<HeroManager>();
            
            _mHero = hero;
            hero.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
            _mHero.mCurrentHealthPoints = _mHero.mMaxHealthPoints;
            _mHero.mCurrentManaPoints = _mHero.mMaxManaPoints;
            _mHero.mNavMeshAgent.SetDestination(heroManager.mHeroSpawner.position);
        }

        public void UpdateState()
        {
            if (_mHero.mCurrentHealthPoints <= 0)
            {
                _mHero.SetState(new DeadHeroState());
            }
        }

        public void Attack(IAttackable target)
        {
            int damageAmount = 60 + (30 * _mHero.GetUpdatedStrength());
            target.TakeDamage(damageAmount);

            float appliedCooldown = _mHero.mAttackCooldown;
            if (_mHero.mAbilityAttackSpeedBoost != 0)
                appliedCooldown = appliedCooldown / _mHero.mAbilityAttackSpeedBoost;
            _mHero.mNextAttackTimestamp = Time.time + appliedCooldown;
        }

        public void TakeDamage(int damageAmount)
        {
            _mHero.mCurrentHealthPoints -= damageAmount;
        }

        public void UpdateNavAgentDestination(NavMeshAgent navMeshAgent)
        {
            // D'abord, si on a une cible, on va continuer de la poursuivre
            if (_mHero.mTarget != null)
            {
                navMeshAgent.SetDestination(_mHero.mTarget.GetPosition());
            }
        
            // `!EventSystem.current.IsPointerOverGameObject()` empêche de prendre en compte les clics sur l'UI
            if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    Monster clickedMonster = hitInfo.transform.gameObject.GetComponent<Monster>();
                    
                    if (clickedMonster)
                    {
                        _mHero.mTarget = clickedMonster;
                    }
                    else
                    {
                        navMeshAgent.SetDestination(hitInfo.point);
                        _mHero.mTarget = null;
                    }
                }
            }
            
            // Si le Hero est assez près de sa cible pour l'attaquer, il s'arrête
            if (_mHero.mTarget != null)
            {
                float distanceFromTarget = Vector3.Distance(_mHero.GetPosition(), _mHero.mTarget.GetPosition());
                if (distanceFromTarget <= _mHero.mAttackRange)
                {
                    navMeshAgent.SetDestination(_mHero.GetPosition());
                    _mHero.transform.LookAt(_mHero.mTarget.GetPosition(), Vector3.up);
                }
            }
        }

        #endregion


    }
}