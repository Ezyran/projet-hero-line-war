using HeroLineWar.Base;
using HeroLineWar.Managers;
using UnityEngine;
using UnityEngine.AI;

namespace HeroLineWar.Creatures
{
    public class DeadMonsterState : IMonsterState
    {
        #region Variables

        private Monster _mMonster;

        #endregion

        #region Implemented methods
        
        public void InitState(Monster monster)
        {
            _mMonster = monster;
            
            // Récompense du joueur
            GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            int reward = monster.mReward * monster.mLevel;
            gameManager.mMoney += reward;
            gameManager.mHeroManager.GiveExperiencePointsToHero(reward);
        }
        
        public void UpdateState()
        {
            // Un Monster mort ne fait rien de spécial
        }

        public void Attack(IAttackable target)
        {
            // Un Monster mort n'attaque pas
        }

        public void TakeDamage(int damageAmount)
        {
            // Un Monster mort ne prend pas de dégats
        }

        public void UpdateNavAgentDestination(NavMeshAgent navMeshAgent)
        {
            // On immobilise le Monster en settant sa destination à sa propre position
            navMeshAgent.SetDestination(_mMonster.transform.position);
        }

        #endregion
    }
}
