using HeroLineWar.Base;
using HeroLineWar.Managers;
using UnityEngine;
using UnityEngine.AI;

namespace HeroLineWar.Creatures
{
    public class DeadHeroState : IHeroState
    {
        #region Variables
        
        public float mRespawnTimestamp;

        private Hero _mHero;

        #endregion

        #region Implemented methods

        public void InitState(Hero hero)
        {
            _mHero = hero;
            _mHero.mTarget = null;
            // Disparition du modèle du Hero
            hero.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
            
            // Initialisation du cooldown de respawn
            float currentTime = Time.time;
            mRespawnTimestamp = currentTime + 10;
        }

        public void UpdateState()
        {
            float currentTime = Time.time;
            if (currentTime >= mRespawnTimestamp)
            {
                HeroManager heroManager = GameObject.Find("GameManager").GetComponent<GameManager>().mHeroManager;
                heroManager.RespawnHero();
                _mHero.SetState(new AliveHeroState());
            }
        }

        public void Attack(IAttackable target)
        {
            // Un Hero mort n'attaque pas
        }

        public void TakeDamage(int damageAmount)
        {
            // Un Hero mort ne peut pas être attaqué
        }

        public void UpdateNavAgentDestination(NavMeshAgent navMeshAgent)
        {
            navMeshAgent.SetDestination(_mHero.transform.position);
        }

        #endregion
    }
}