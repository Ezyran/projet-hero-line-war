using HeroLineWar.Base;
using UnityEngine.AI;

namespace HeroLineWar.Creatures
{
    public interface IMonsterState
    {
        public void InitState(Monster monster);
        public void UpdateState();
        public void Attack(IAttackable target);
        public void TakeDamage(int damageAmount);
        public void UpdateNavAgentDestination(NavMeshAgent navMeshAgent);
    }
}
