using System;
using HeroLineWar.Base;
using UnityEngine;

namespace HeroLineWar.Creatures
{
    [RequireComponent(typeof(Outline))]
    public class Monster : Creature
    {
        #region Variables

        public HeroDetector mHeroDetector;
        public IAttackable mDefaultTarget;
        public int mReward;
        public int mCost;
        public IMonsterState mState;

        private Outline _mOutline;
        
        #endregion
        
        #region Built-in methods
        // Start is called before the first frame update
        protected new void Start()
        {
            base.Start();
            
            // Initialisation de l'état Vivant
            SetState(new AliveMonsterState());
            
            // Ajustement des stats en fonction du niveau
            mDamage *= (int)(1 + 0.40f * mLevel);
            mMovementSpeed *= 1 + 0.10f * mLevel;
            mMaxHealthPoints *= (int) (1 + 0.40f * mLevel);
            
            // Initialisation de la zone de détection du Hero
            SphereCollider detectionArea = mHeroDetector.GetComponent<SphereCollider>();
            detectionArea.isTrigger = true;
            detectionArea.radius = 6;
            
            // Initialisation de la cible par défaut
            mTarget = mDefaultTarget;
            
            // Initialisation de l'outline
            _mOutline = GetComponent<Outline>();
            _mOutline.enabled = false;
        }

        // Update is called once per frame
        protected new void Update()
        {
            mState.UpdateState();
            
            base.Update();
            
            // Gestion de la détection du Hero
            TryToTargetHero();
                
            // Gestion de l'attaque
            TryToAttack();
        }
        
        #endregion

        #region Implemented methods
        
        public override void Attack(IAttackable target)
        {
            mState.Attack(target);
        }

        public override void TakeDamage(int damageAmount)
        {
            mState.TakeDamage(damageAmount);
        }

        protected override void UpdateNavAgentDestination()
        {
            mState.UpdateNavAgentDestination(mNavMeshAgent);
        }

        public override bool IsTargetable()
        {
            return mState is AliveMonsterState;
        }

        #endregion

        #region Custom methods

        public void SetState(IMonsterState newState)
        {
            mState = newState;
            mState.InitState(this);
        }

        public Outline GetMonsterOutline()
        {
            return _mOutline;
        }

        /** Essaye de cibler le Hero s'il se trouve à portée */
        private void TryToTargetHero()
        {
            mTarget = mDefaultTarget;
            if (mHeroDetector.mDetectedHero && mHeroDetector.mDetectedHero.IsTargetable())
                mTarget = mHeroDetector.mDetectedHero;
        }
        
        /** Essaye d'attaquer la cible si elle se trouve à portée et si le cooldown d'attaque est écoulé */
        private void TryToAttack()
        {
            float distanceFromTarget = Vector3.Distance(transform.position, mTarget.GetPosition());
            float currentTime = Time.time;
            if (distanceFromTarget <= mAttackRange && currentTime >= mNextAttackTimestamp)
            {
                Attack(mTarget);
            }
        }

        #endregion
    }
}
