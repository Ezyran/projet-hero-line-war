using HeroLineWar.Base;
using UnityEngine;
using UnityEngine.AI;

namespace HeroLineWar.Creatures
{
    [RequireComponent(typeof(NavMeshAgent))]
    public abstract class Creature : MonoBehaviour, IAttackable
    {
        #region Variables

        [Header("Creature Properties")] 
        public IAttackable mTarget;

        [Header("Creature Stats")] 
        public int mMaxHealthPoints = 100;
        public int mCurrentHealthPoints;
        public float mMovementSpeed = 1f;
        public float mDamage = 1f;
        public int mLevel = 1;
        public float mAttackRange = 1f;
        public float mAttackCooldown = 2f;
        public float mNextAttackTimestamp;
        public NavMeshAgent mNavMeshAgent;

        #endregion
        
        #region Built-in methods

        // Start is called before the first frame update
        protected void Start()
        {
            mCurrentHealthPoints = mMaxHealthPoints;
            
            mNavMeshAgent = GetComponent<NavMeshAgent>();
            mNavMeshAgent.speed *= mMovementSpeed;
        }

        // Update is called once per frame
        protected void Update()
        {
            UpdateNavAgentDestination();
        }

        #endregion
        
        #region Implemented methods

        public virtual int GetCurrentHealthPoints()
        {
            return mCurrentHealthPoints;
        }

        public virtual int GetMaxHealthPoints()
        {
            return mMaxHealthPoints;
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        #endregion

        #region Abstract methods

        protected abstract void UpdateNavAgentDestination();
        public abstract void Attack(IAttackable target);
        public abstract void TakeDamage(int amount);
        public abstract bool IsTargetable();

        #endregion
    }


}
