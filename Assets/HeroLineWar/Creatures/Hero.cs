using System;
using System.Collections.ObjectModel;
using HeroLineWar.Abilities;
using HeroLineWar.Base;
using HeroLineWar.Items;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

namespace HeroLineWar.Creatures
{
    [RequireComponent(typeof(AbilityManager))]
    public class Hero : Creature
    {
        #region Variables

        // Seuil de base pour passer au niveau supérieur
        public static int InitialLevelUpThreshold = 100;
        
        [Header("Hero Base Stats")]
        public int mCurrentManaPoints;
        public int mMaxManaPoints = 100;
        public int mStrength = 1;
        public int mAgility = 1;
        public int mIntelligence = 1;
        public int mStamina = 1;
        public int mExperiencePoints = 0;
        public IHeroState mState;
        public Collection<Item> mItemList;

        [Header("Hero Enhanced Stats")] 
        public int mAbilityAttackSpeedBoost;
        public int mAbilityMovementSpeedBoost;
        public int mAbilityStaminaBoost;
        public int mAbilityStrengthBoost;

        [Header("Hero Components")]
        public Collection<Ability> mAbilities;
        private AbilityManager _mAbilityManager;

        #endregion

        #region Built-in methods
        // Start is called before the first frame update
        protected new void Start()
        {
            mNavMeshAgent = GetComponent<NavMeshAgent>();
            _mAbilityManager = GetComponent<AbilityManager>();
            base.Start();
            
            SetState(new AliveHeroState());
            
            mCurrentManaPoints = mMaxManaPoints;
        }

        // Update is called once per frame
        protected new void Update()
        {
            // Changement de la cible si l'actuelle meurt
            UpdateTarget();
            
            mState.UpdateState();
            
            base.Update();
            
            // Gestion de la montée de niveau
            int currentLevelUpthreshold = InitialLevelUpThreshold * mLevel;
            if (mExperiencePoints >= currentLevelUpthreshold)
            {
                LevelUp();
            }
            
            // Lancement d'une capacité
            HandleAbilities();
            
            // Update des boosts de stat des capacités
            UpdateAbilityBoosts();
            
            // Correction niveaux de vie et de mana
            CorrectHealthMana();

            TryToAttack();
        }

        #endregion

        #region Implemented methods
    
        public override void Attack(IAttackable target)
        {
            mState.Attack(target);
        }

        public override void TakeDamage(int amount)
        {
            mState.TakeDamage(amount);
        }

        public override bool IsTargetable()
        {
            return mState is AliveHeroState;
        }

        protected override void UpdateNavAgentDestination()
        {
            mState.UpdateNavAgentDestination(mNavMeshAgent);
        }

        public override int GetMaxHealthPoints()
        {
            return mMaxHealthPoints * GetUpdatedStamina();
        }
        

        #endregion

        #region Custom methods

        void CorrectHealthMana()
        {
            int maxMana = GetMaxManaPoints();
            if (mCurrentManaPoints > maxMana)
                mCurrentManaPoints = maxMana;
            
            int maxHealth = GetMaxHealthPoints();
            if (mCurrentHealthPoints > maxHealth)
                mCurrentHealthPoints = maxHealth;
        }

        public int GetMaxManaPoints()
        {
            return mMaxHealthPoints * GetUpdatedIntelligence();
        }

        public void SetState(IHeroState newState)
        {
            mState = newState;
            newState.InitState(this);
        }
        
        public void LevelUp()
        {
            mStrength++;
            mAgility++;
            mIntelligence++;
            mStamina++;
            mExperiencePoints = 0;
            mLevel++;

            mCurrentManaPoints = GetMaxManaPoints();
            mCurrentHealthPoints = GetMaxHealthPoints();
        }

        private void HandleAbilities()
        {
            // Lancement d'une nouvelle capacité
            if (!_mAbilityManager.IsAbilityStillExecuting())
            {
                float currentTime = Time.time;
                Ability launchedAbility = null;
                if (Input.GetAxis("Ability1") >= 1f)
                {
                    if (currentTime >= mAbilities[0].mCooldownEndTimestamp &&
                        mCurrentManaPoints >= mAbilities[0].mManaCost)
                    {
                        launchedAbility = (Ability) mAbilities[0].Clone();
                        mAbilities[0].mCooldownEndTimestamp = Time.time + mAbilities[0].mCooldown;
                    }
                } 
                else if (Input.GetAxis("Ability2") >= 1f)
                {
                    if (currentTime >= mAbilities[1].mCooldownEndTimestamp &&
                        mCurrentManaPoints >= mAbilities[1].mManaCost)
                    {
                        launchedAbility = (Ability) mAbilities[1].Clone();
                        mAbilities[1].mCooldownEndTimestamp = Time.time + mAbilities[1].mCooldown;
                    }
                }
                else if (Input.GetAxis("Ability3") >= 1f)
                {
                    if (currentTime >= mAbilities[2].mCooldownEndTimestamp &&
                        mCurrentManaPoints >= mAbilities[2].mManaCost)
                    {
                        launchedAbility = (Ability) mAbilities[2].Clone();
                        mAbilities[2].mCooldownEndTimestamp = Time.time + mAbilities[2].mCooldown;
                    }
                }
                else if (Input.GetAxis("Ability4") >= 1f)
                {
                    if (currentTime >= mAbilities[3].mCooldownEndTimestamp &&
                        mCurrentManaPoints >= mAbilities[3].mManaCost)
                    {
                        launchedAbility = (Ability) mAbilities[3].Clone();
                        mAbilities[3].mCooldownEndTimestamp = Time.time + mAbilities[3].mCooldown;
                    }
                }

                if (launchedAbility != null)
                {
                    _mAbilityManager.SetStrategy(launchedAbility);
                    mCurrentManaPoints -= launchedAbility.mManaCost;
                }
            }
        }

        private void UpdateTarget()
        {
            if (mTarget != null)
            {
                Monster targetedMonster = (Monster) mTarget;
                if (targetedMonster.mState is DeadMonsterState)
                {
                    mTarget = null;

                    float minDistance = float.MaxValue;
                    Collider[] colliders = Physics.OverlapSphere(transform.position, 1f);
                    foreach (var collider in colliders)
                    {
                        Monster currentMonster = collider.GetComponent<Monster>();
                        if (currentMonster && Vector3.Distance(transform.position, currentMonster.GetPosition()) < minDistance)
                        {
                            mTarget = currentMonster;
                        } 
                    }
                }
            }
        }

        public int GetUpdatedStrength()
        {
            int res = mStrength + mAbilityStrengthBoost;
            foreach (var item in mItemList)
            {
                if (item is Equipment)
                {
                    Equipment equipment = (Equipment) item;
                    res += equipment.mStrengthBoost;
                }
            }

            return res;
        }
        
        public int GetUpdatedAgility()
        {
            int res = mAgility;
            foreach (var item in mItemList)
            {
                if (item is Equipment)
                {
                    Equipment equipment = (Equipment) item;
                    res += equipment.mAgilityBoost;
                }
            }

            return res;
        }
        
        public int GetUpdatedIntelligence()
        {
            int res = mIntelligence;
            foreach (var item in mItemList)
            {
                if (item is Equipment)
                {
                    Equipment equipment = (Equipment) item;
                    res += equipment.mIntelligenceBoost;
                }
            }

            return res;
        }
        
        public int GetUpdatedStamina()
        {
            int res = mStamina + mAbilityStaminaBoost;
            foreach (var item in mItemList)
            {
                if (item is Equipment)
                {
                    Equipment equipment = (Equipment) item;
                    res += equipment.mStaminaBoost;
                }
            }

            return res;
        }

        public void UpdateAbilityBoosts()
        {
            if (_mAbilityManager.IsAbilityStillExecuting()) return;
            mAbilityStaminaBoost = 0;
            mAbilityStrengthBoost = 0;
            mAbilityAttackSpeedBoost = 0;
            mAbilityMovementSpeedBoost = 0;
        }

        public void TryToAttack()
        {
            if (mTarget != null)
            {
                float distanceFromTarget = Vector3.Distance(transform.position, mTarget.GetPosition());
                float currentTime = Time.time;
                if (distanceFromTarget <= mAttackRange && currentTime >= mNextAttackTimestamp)
                {
                    Attack(mTarget);
                }
            }
        }

        #endregion

    }
}
