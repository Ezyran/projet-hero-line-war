using HeroLineWar.Base;
using UnityEngine;
using UnityEngine.AI;

namespace HeroLineWar.Creatures
{
    public class AliveMonsterState : IMonsterState
    {
        #region Variables

        private Monster _mMonster;

        #endregion

        #region Implemented methods

        public void InitState(Monster monster)
        {
            _mMonster = monster;
        }

        public void UpdateState()
        {
            if (_mMonster.mCurrentHealthPoints <= 0)
            {
                _mMonster.SetState(new DeadMonsterState()); 
            }
        }

        public void Attack(IAttackable target)
        {
            target.TakeDamage((int)_mMonster.mDamage);
            _mMonster.mNextAttackTimestamp = Time.time + _mMonster.mAttackCooldown;
        }

        public void TakeDamage(int damageAmount)
        {
            _mMonster.mCurrentHealthPoints -= damageAmount;
        }

        public void UpdateNavAgentDestination(NavMeshAgent navMeshAgent)
        {
            if (_mMonster.mTarget != null)
            {
                navMeshAgent.destination = _mMonster.mTarget.GetPosition();
            }
            
            // Si le Monster est assez près de sa cible pour l'attaquer, il s'arrête
            float distanceFromTarget = Vector3.Distance(_mMonster.GetPosition(), navMeshAgent.destination);
            if (distanceFromTarget <= _mMonster.mAttackRange)
            {
                navMeshAgent.destination = _mMonster.GetPosition();
            }
        }

        #endregion
    }
}
