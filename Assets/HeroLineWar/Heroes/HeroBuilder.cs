using UnityEngine;

namespace HeroLineWar.Heroes
{
    public interface HeroBuilder
    {
        public void SetHeroPrefab();
        public void SetHeroAbilities();

        public void InitItemList();
        public void ResetResult();
        public GameObject GetResult();
    }
}
