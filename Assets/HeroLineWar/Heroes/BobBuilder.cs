using System.Collections.ObjectModel;
using HeroLineWar.Abilities;
using HeroLineWar.Creatures;
using HeroLineWar.Items;
using UnityEngine;

namespace HeroLineWar.Heroes
{
    public class BobBuilder : HeroBuilder
    {
        private GameObject _mResultHero;
        private GameObject _mBobPrefab;
        
        public BobBuilder(GameObject bobPrefab)
        {
            _mBobPrefab = bobPrefab;
        }

        public void SetHeroPrefab()
        {
            _mResultHero = _mBobPrefab;
        }

        public void SetHeroAbilities()
        {
            Hero hero = _mResultHero.GetComponent<Hero>();
            hero.mAbilities = new Collection<Ability>
            {
                new DamageZoneAbility(hero, 2f, 0f, 20),
                new RangedDamageAbility(hero, 2f, 0f, 40),
                new DestinationTeleportationAbility(hero, 10f, 0f, 100),
                new SpawnTeleportationAbility(hero, 30f, 0f, 80)
            };
        }

        public void InitItemList()
        {
            Hero hero = _mResultHero.GetComponent<Hero>();
            hero.mItemList = new Collection<Item>();
        }

        public void ResetResult()
        {
            _mResultHero = _mBobPrefab;
        }

        public GameObject GetResult()
        {
            return _mResultHero;
        }
    }
}