using System.Collections.ObjectModel;
using HeroLineWar.Abilities;
using HeroLineWar.Creatures;
using HeroLineWar.Items;
using UnityEngine;

namespace HeroLineWar.Heroes
{
    public class AliceBuilder : HeroBuilder
    {
        private GameObject _mResultHero;
        private GameObject _mAlicePrefab;
        
        public AliceBuilder(GameObject alicePrefab)
        {
            _mAlicePrefab = alicePrefab;
        }

        public void SetHeroPrefab()
        {
            _mResultHero = _mAlicePrefab;
        }

        public void SetHeroAbilities()
        {
            Hero hero = _mResultHero.GetComponent<Hero>();
            hero.mAbilities = new Collection<Ability>
            {
                new AttackSpeedBoostAbility(hero, 8f, 5f, 20),
                new MovementSpeedBoostAbility(hero, 8f, 5f, 10),
                new StaminaBoostAbility(hero, 8f, 5f, 40),
                new StrengthBoostAbility(hero, 8f, 5f, 100)
            };
        }

        public void InitItemList()
        {
            Hero hero = _mResultHero.GetComponent<Hero>();
            hero.mItemList = new Collection<Item>();
        }

        public void ResetResult()
        {
            _mResultHero = _mAlicePrefab;
        }

        public GameObject GetResult()
        {
            return _mResultHero;
        }
    }
}