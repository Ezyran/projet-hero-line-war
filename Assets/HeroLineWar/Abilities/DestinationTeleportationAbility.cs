using HeroLineWar.Creatures;
using UnityEngine;

namespace HeroLineWar.Abilities
{
    public class DestinationTeleportationAbility : Ability
    {
        public DestinationTeleportationAbility(Hero hero, float cooldown, float duration, int manaCost) : base(hero, cooldown, duration, manaCost)
        {
        }

        public override void Execute()
        {
            mAlreadyLaunchedOnce = true;
            Vector3 nextPosition = _mHero.mNavMeshAgent.destination;
            _mHero.transform.position = nextPosition;
        }

        public override object Clone()
        {
            DestinationTeleportationAbility clone = new DestinationTeleportationAbility(_mHero, mCooldown, mDuration, mManaCost);
            return clone;
        }
    }
}