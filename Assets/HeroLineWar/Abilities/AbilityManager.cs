using System;
using UnityEngine;

namespace HeroLineWar.Abilities
{
    public class AbilityManager : MonoBehaviour
    {
        private Ability _mCurrentAbility;

        public void Update()
        {
            if (IsAbilityStillExecuting())
            {
                // Sécurité pour empêcher le lancement multiple des capacités d'une durée nulle
                if (_mCurrentAbility.mDuration == 0f && _mCurrentAbility.mAlreadyLaunchedOnce) return;
                _mCurrentAbility.Execute();
            }
        }

        public void SetStrategy(Ability newAbility)
        {
            _mCurrentAbility = newAbility;
            _mCurrentAbility.SetEndTimestamp();
        }

        public bool IsAbilityStillExecuting()   
        {
            if (_mCurrentAbility == null) return false;
            float currentTime = Time.time;
            return currentTime <= _mCurrentAbility.mEndTimestamp;
        }
    }
}
