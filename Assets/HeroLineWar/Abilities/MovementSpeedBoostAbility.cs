using HeroLineWar.Creatures;

namespace HeroLineWar.Abilities
{
    public class MovementSpeedBoostAbility : Ability
    {
        public MovementSpeedBoostAbility(Hero hero, float cooldown, float duration, int manaCost) : base(hero, cooldown, duration, manaCost)
        {
        }

        public override void Execute()
        {
            mAlreadyLaunchedOnce = true;
            _mHero.mAbilityMovementSpeedBoost = 2;
        }

        public override object Clone()
        {
            MovementSpeedBoostAbility clone = new MovementSpeedBoostAbility(_mHero, mCooldown, mDuration, mManaCost);
            return clone;
        }
    }
}