using System;
using HeroLineWar.Creatures;
using UnityEngine;

namespace HeroLineWar.Abilities
{
    public abstract class Ability : ICloneable
    {
        #region Variables

        public int mManaCost;
        public float mCooldown;
        public float mCooldownEndTimestamp;
        public float mDuration;
        public float mEndTimestamp;
        public bool mAlreadyLaunchedOnce;
        protected Hero _mHero;

        #endregion

        public Ability(Hero hero, float cooldown, float duration, int manaCost)
        {
            mManaCost = manaCost;
            mCooldown = cooldown;
            mDuration = duration;
            mAlreadyLaunchedOnce = false;
            _mHero = hero;
        }

        public void SetEndTimestamp()
        {
            mEndTimestamp = Time.time + mDuration;
        }

        public abstract void Execute();
        public abstract object Clone();
    }
}
