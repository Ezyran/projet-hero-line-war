using HeroLineWar.Base;
using HeroLineWar.Creatures;

namespace HeroLineWar.Abilities
{
    public class RangedDamageAbility : Ability
    {
        private float _mRange;
        
        public RangedDamageAbility(Hero hero, float cooldown, float duration, int manaCost) : base(hero, cooldown, duration, manaCost) { }

        public override void Execute()
        {
            mAlreadyLaunchedOnce = true;
            if (_mHero.mTarget != null)
            {
                IAttackable heroTarget = _mHero.mTarget;
                int appliedDamage = 20 * (_mHero.mStrength + _mHero.mAgility);
                heroTarget.TakeDamage(appliedDamage);
            }
        }

        public override object Clone()
        {
            RangedDamageAbility clone = new RangedDamageAbility(_mHero, mCooldown, mDuration, mManaCost);
            return clone;
        }
    }
}