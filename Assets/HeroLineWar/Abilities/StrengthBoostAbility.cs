using HeroLineWar.Creatures;

namespace HeroLineWar.Abilities
{
    public class StrengthBoostAbility : Ability
    {
        public StrengthBoostAbility(Hero hero, float cooldown, float duration, int manaCost) : base(hero, cooldown, duration, manaCost)
        {
        }

        public override void Execute()
        {
            mAlreadyLaunchedOnce = true;
            _mHero.mAbilityStrengthBoost = 6;
        }

        public override object Clone()
        {
            StrengthBoostAbility clone = new StrengthBoostAbility(_mHero, mCooldown, mDuration, mManaCost);
            return clone;
        }
    }
}