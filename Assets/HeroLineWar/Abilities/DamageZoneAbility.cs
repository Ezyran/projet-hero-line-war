using HeroLineWar.Creatures;
using UnityEngine;

namespace HeroLineWar.Abilities
{
    public class DamageZoneAbility : Ability
    {
        public DamageZoneAbility(Hero hero, float cooldown, float duration, int manaCost) : base(hero, cooldown, duration, manaCost) {}
        
        public override void Execute()
        {
            mAlreadyLaunchedOnce = true;
            Collider[] colliders = Physics.OverlapSphere(_mHero.transform.position, 2f);
            foreach (var collider in colliders)
            {
                Monster currentMonster = collider.GetComponent<Monster>();
                if (currentMonster)
                {
                    int appliedDamage = _mHero.mStrength * 33;
                    currentMonster.TakeDamage(appliedDamage);
                }
            }
        }

        public override object Clone()
        {
            DamageZoneAbility clone = new DamageZoneAbility(_mHero, mCooldown, mDuration, mManaCost);
            return clone;
        }
    }
}