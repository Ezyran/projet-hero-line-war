using HeroLineWar.Creatures;
using HeroLineWar.Managers;
using UnityEngine;

namespace HeroLineWar.Abilities
{
    public class SpawnTeleportationAbility : Ability
    {
        public SpawnTeleportationAbility(Hero hero, float cooldown, float duration, int manaCost) : base(hero, cooldown, duration, manaCost)
        {
        }

        public override void Execute()
        {
            mAlreadyLaunchedOnce = true;
            GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            Vector3 nextPosition = gameManager.mHeroManager.mHeroSpawner.position;
            _mHero.transform.position = nextPosition;
        }

        public override object Clone()
        {
            SpawnTeleportationAbility clone = new SpawnTeleportationAbility(_mHero, mCooldown, mDuration, mManaCost);
            return clone;
        }
    }
}