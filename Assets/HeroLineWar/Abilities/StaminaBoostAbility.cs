using HeroLineWar.Creatures;

namespace HeroLineWar.Abilities
{
    public class StaminaBoostAbility : Ability
    {
        public StaminaBoostAbility(Hero hero, float cooldown, float duration, int manaCost) : base(hero, cooldown, duration, manaCost)
        {
        }

        public override void Execute()
        {
            mAlreadyLaunchedOnce = true;
            _mHero.mAbilityStaminaBoost = 6;
        }

        public override object Clone()
        {
            StaminaBoostAbility clone = new StaminaBoostAbility(_mHero, mCooldown, mDuration, mManaCost);
            return clone;
        }
    }
}