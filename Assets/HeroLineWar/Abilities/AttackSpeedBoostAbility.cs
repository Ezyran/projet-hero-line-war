using HeroLineWar.Creatures;

namespace HeroLineWar.Abilities
{
    public class AttackSpeedBoostAbility : Ability
    {
        public AttackSpeedBoostAbility(Hero hero, float cooldown, float duration, int manaCost) : base(hero, cooldown, duration, manaCost)
        {
        }

        public override void Execute()
        {
            mAlreadyLaunchedOnce = true;
            _mHero.mAbilityAttackSpeedBoost = 4;
        }

        public override object Clone()
        {
            AttackSpeedBoostAbility clone = new AttackSpeedBoostAbility(_mHero, mCooldown, mDuration, mManaCost);
            return clone;
        }
    }
}