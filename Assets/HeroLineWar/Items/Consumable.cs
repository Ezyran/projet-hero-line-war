
using HeroLineWar.Creatures;

namespace HeroLineWar.Items
{
    public abstract class Consumable : Item
    {
        public Hero mHero;

        public Consumable(string name, int cost, Hero hero) : base(name, cost)
        {
            mName = name;
            mCost = cost;
            mHero = hero;
        }
        public abstract void Consume();
    }
}
