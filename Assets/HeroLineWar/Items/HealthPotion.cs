using HeroLineWar.Creatures;

namespace HeroLineWar.Items
{
    public class HealthPotion : Consumable
    {
        public HealthPotion(string name, int cost, Hero hero) : base(name, cost, hero) { }

        public override void Consume()
        {
            mHero.mCurrentHealthPoints += 40;
        }

        public override object Clone()
        {
            return new HealthPotion(mName, mCost, mHero);
        }
    }
}
