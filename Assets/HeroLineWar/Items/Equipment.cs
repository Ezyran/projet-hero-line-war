
using UnityEngine.AI;

namespace HeroLineWar.Items
{
    public class Equipment : Item
    {
        public int mStrengthBoost;
        public int mAgilityBoost;
        public int mIntelligenceBoost;
        public int mStaminaBoost;

        public Equipment(string name, int cost, int strengthBoost, int agilityBoost, int intelligenceBoost, int staminaBoost) : base(name, cost)
        {
            mName = name;
            mCost = cost;
            mStrengthBoost = strengthBoost;
            mAgilityBoost = agilityBoost;
            mIntelligenceBoost = intelligenceBoost;
            mStaminaBoost = staminaBoost;
        }

        public override object Clone()
        {
            return new Equipment(mName, mCost, mStrengthBoost, mAgilityBoost, mIntelligenceBoost, mStaminaBoost);
        }
    }
}
