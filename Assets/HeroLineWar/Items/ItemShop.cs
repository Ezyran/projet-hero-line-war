using System.Collections.ObjectModel;
using HeroLineWar.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HeroLineWar.Items
{
    public class ItemShop : MonoBehaviour
    {
        public Button[] mItemButtons;
        private Collection<Item> _mItemList;
    
        // Start is called before the first frame update
        void Start()
        {
            InitItemList();
            InitItemButtons();
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        void InitItemList()
        {
            _mItemList = new Collection<Item>
            {
                new Equipment("Epée", 200, 2, 0, 0, 0),
                new Equipment("Bottes", 200, 0, 2, 0, 0),
                new Equipment("Lunettes", 200, 0,0,2,0),
                new Equipment("Armure", 200, 0,0,0,2),
                new HealthPotion("Potion HP", 50, null),
                new ManaPotion("Potion MP", 50, null)
            };
        }

        void InitItemButtons()
        {
            foreach (var button in mItemButtons)
            {
                button.onClick.AddListener(OnItemButtonClick);
            }
        }

        void BuyItem(Item item)
        {
            GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            if (item.mCost <= gameManager.mMoney)
            {
                // Envoi de l'item cloné au HeroManager
                HeroManager heroManager = gameManager.mHeroManager;
                heroManager.GiveItem((Item)item.Clone());
                // Vol de l'argent au Hero
                gameManager.mMoney -= item.mCost;
            }
        }

        void OnItemButtonClick()
        {
            string buttonName =  EventSystem.current.currentSelectedGameObject.name;
            Item retrievedItem = RetrieveItemByName(buttonName);

            if (retrievedItem != null)
            {
                BuyItem(retrievedItem);
            }

        }

        Item RetrieveItemByName(string itemName)
        {
            Item res = null;
            foreach (var item in _mItemList)
            {
                if (item.mName == itemName)
                    res = item;
            }
            return res;
        }
    }
}
