using System.Collections;
using System.Collections.Generic;
using HeroLineWar.Creatures;
using HeroLineWar.Items;
using UnityEngine;

public class ManaPotion : Consumable
{
    public ManaPotion(string name, int cost, Hero hero) : base(name, cost, hero) { }

    public override void Consume()
    {
        mHero.mCurrentManaPoints += 40;
    }
    
    public override object Clone()
    {
        return new ManaPotion(mName, mCost, mHero);
    }
}
