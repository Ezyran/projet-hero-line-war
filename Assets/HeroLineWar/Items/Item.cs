
using System;

namespace HeroLineWar.Items
{
    public abstract class Item : ICloneable
    {
        public Item(string name, int cost)
        {
            mName = name;
            mCost = cost;
        }
        
        public string mName;
        public int mCost;

        public abstract object Clone();
    }
}
