using System.Collections.ObjectModel;
using HeroLineWar.Base;
using HeroLineWar.Creatures;
using UnityEngine;

namespace HeroLineWar.Managers
{
    public class MonsterManager : MonoBehaviour
    {
        #region Variables

        public Transform[] mMonsterSpawners;
        
        // Utilisé pour basculer entre les spawners à chaque spawn
        private int _mNextMonsterSpawnersId = 0;
        private Collection<GameObject> _mMonsterInstances;
        
        // Pile des prefabs à intancier
        private Collection<GameObject> _mMonsterToSpawn;
        private float _mNextSpawnTimestamp;

        #endregion
        
        #region Built-in methods
        // Start is called before the first frame update
        void Start()
        {
            _mMonsterToSpawn = new Collection<GameObject>();
            _mMonsterInstances = new Collection<GameObject>();
            _mNextSpawnTimestamp = Time.time + 2f; // Time.time = timestamp actuel
        }   

        // Update is called once per frame
        void Update()
        {
            // Spawn des monstres de la pile
            if (Time.time >= _mNextSpawnTimestamp && _mMonsterToSpawn.Count > 0 )
            {
                Hero hero = GameObject.Find("GameManager").GetComponent<GameManager>().mHeroManager.GetHero();
                float nextSpawnCooldown = 2f - (0.2f * hero.mLevel);
                _mNextSpawnTimestamp = Time.time + nextSpawnCooldown;
                
                CreateMonster(_mMonsterToSpawn[0]);
                _mMonsterToSpawn.RemoveAt(0);
            }
            DeleteDeadMonsters();
        }
        
        #endregion

        #region Custom methods

        public void CreateMonster(GameObject monsterPrefab)
        {
            GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            
            // Sélection du spawner à utiliser, et incrémentation de l'indice du prochain spawner à utiliser
            Transform nextSpawner = mMonsterSpawners[_mNextMonsterSpawnersId];
            _mNextMonsterSpawnersId = (_mNextMonsterSpawnersId + 1) % mMonsterSpawners.Length;

            // Création du GameObject représentant le monstre
            GameObject monsterInstance = Instantiate(monsterPrefab, nextSpawner.position, new Quaternion());
            _mMonsterInstances.Add(monsterInstance);
            
            // Config du monstre
            monsterInstance.GetComponent<Monster>().mDefaultTarget = gameManager.mNexus;
        }

        private void DeleteDeadMonsters()
        {
            // Parcours de la liste d'instances à l'envers pour pouvoir nettoyer sans casser la boucle de lecture
            for (int instanceIdx = _mMonsterInstances.Count - 1; instanceIdx >= 0; instanceIdx--)
            {
                GameObject instance = _mMonsterInstances[instanceIdx];
                Monster monster = instance.GetComponent<Monster>();
                if (monster.mState is DeadMonsterState)
                {
                    _mMonsterInstances.Remove(instance);
                    Destroy(instance);
                }
            }
        }

        public void AddPrefabToSpawnList(GameObject monsterPrefab)
        {
            _mMonsterToSpawn.Add(monsterPrefab);
        }

        #endregion
    }
}
