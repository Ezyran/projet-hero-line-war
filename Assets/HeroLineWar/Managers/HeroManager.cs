using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using HeroLineWar.Abilities;
using HeroLineWar.Base;
using HeroLineWar.Creatures;
using HeroLineWar.Heroes;
using HeroLineWar.Items;
using UnityEngine;
using UnityEngine.UI;

namespace HeroLineWar.Managers
{
    public class HeroManager : MonoBehaviour
    {
        #region Variables

        public int mHeroItemCapacity = 6;
        public float mHeroRegenCooldown = 1f;
        public Transform mHeroSpawner;
        public Image mHeroDestinationMarker;
        public GameObject mBobPrefab;
        public GameObject mAlicePrefab;

        private float _mNextRegenTimestamp;
        private GameObject _mHeroInstance;
        private Monster _mHeroTarget;

        #endregion
        
        #region Built-in methods

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            UpdateHeroTarget();
            if (Time.time >= _mNextRegenTimestamp)
            {
                RegenHealth();
                RegenMana();
                _mNextRegenTimestamp = Time.time + mHeroRegenCooldown;
            }
        }

        #endregion
        
        #region Custom methods

        public void InitHero()
        {
            string selectedHeroName = PlayerPrefs.GetString("HeroName", "Bob");
            
            GameObject selectedHeroPrefab = null;
            HeroBuilder heroBuilder = null;
            if (selectedHeroName == "Bob")
            {
                selectedHeroPrefab = Instantiate(mBobPrefab,mHeroSpawner.transform.position, new Quaternion());
                heroBuilder = new BobBuilder(selectedHeroPrefab);
            } 
            else if (selectedHeroName == "Alice")
            {
                selectedHeroPrefab = Instantiate(mAlicePrefab,mHeroSpawner.transform.position, new Quaternion());
                heroBuilder = new AliceBuilder(selectedHeroPrefab);
            }

            heroBuilder.SetHeroPrefab();
            heroBuilder.SetHeroAbilities();
            heroBuilder.InitItemList();
            _mHeroInstance = heroBuilder.GetResult();
        }

        public void GiveExperiencePointsToHero(int experiencePoints)
        {
            _mHeroInstance.GetComponent<Hero>().mExperiencePoints += experiencePoints;
        }

        public Hero GetHero()
        {
            return _mHeroInstance.GetComponent<Hero>();
        }

        public void RespawnHero()
        {
            _mHeroInstance.transform.position = mHeroSpawner.position;
        }

        public void RegenHealth()
        {
            Hero hero = GetHero();
            hero.mCurrentHealthPoints += hero.mLevel * hero.GetUpdatedStamina() * 2;
        }

        public void RegenMana()
        {
            Hero hero = GetHero();
            hero.mCurrentManaPoints += hero.mLevel * hero.GetUpdatedIntelligence() * 2;
        }

        public void UpdateHeroTarget()
        {
            Monster oldTarget = _mHeroTarget;
            Monster newTarget = (Monster) GetHero().mTarget;
            
            // Lorsque le Hero change de cible,
            if (newTarget != oldTarget)
            {
                // Si la cible précédente n'est pas null,
                if (oldTarget)
                {
                    // Enlever l'highlight sur l'ancienne cible
                    oldTarget.GetMonsterOutline().enabled = false;
                }
                
                // Si la cible actuelle est un monstre
                if (newTarget)
                {
                    // Ajouter l'highlight sur la nouvelle cible
                    newTarget.GetMonsterOutline().enabled = true;
                }

                _mHeroTarget = newTarget;
            }
            
            // Dans tous les cas, positionner un marqueur sur la destination du Hero
            mHeroDestinationMarker.transform.position = GetHero().mNavMeshAgent.destination + new Vector3(0f, 0.1f, 0f);

        }

        public void GiveItem(Item item)
        {
            Hero hero = GetHero();
            if (hero.mItemList.Count < mHeroItemCapacity)
            {
                if (item is Consumable)
                {
                    Consumable consumable = (Consumable) item;
                    consumable.mHero = hero;
                    hero.mItemList.Add(consumable);
                }
                else if (item is Equipment)
                {
                    Equipment equipment = (Equipment) item;
                    hero.mItemList.Add(equipment);
                }
            }
        }

        #endregion
    }
}
