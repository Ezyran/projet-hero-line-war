using HeroLineWar.Base;
using HeroLineWar.Items;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HeroLineWar.Managers
{
    public class GameManager : MonoBehaviour
    {
        # region Variables
        
        [Header("Game Properties")] 
        public Nexus mNexus;
        public int mMoney;

        [Header("Managers")] 
        public HeroManager mHeroManager;
        public MonsterManager mMonsterManager;

        [Header("Shops")]
        public ItemShop mItemShop;
        
        private float _mNextMoneyIncrementTimestamp;
        
        #endregion

        #region Built-in methods

        // Start is called before the first frame update
        void Start()
        {
            mHeroManager.InitHero();
            _mNextMoneyIncrementTimestamp = Time.time + 1;
        }

        // Update is called once per frame
        void Update()
        {
            IncrementMoney();
            if (IsGameOver())
            {
                SceneManager.LoadScene(0);
            }
        }  

        #endregion

        #region Custom methods

        private void IncrementMoney()
        {
            float currentTime = Time.time;
            if (currentTime >= _mNextMoneyIncrementTimestamp)
            {
                int currentHeroLevel = mHeroManager.GetHero().mLevel;
                mMoney += currentHeroLevel * 2;
                _mNextMoneyIncrementTimestamp = currentTime + 1;
            }
        }

        bool IsGameOver()
        {
            if (mNexus.GetCurrentHealthPoints() <= 0)
            {
                return true;
            }
            return false;
        }
        
        #endregion
    }
}
