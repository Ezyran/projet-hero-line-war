using System.Collections.ObjectModel;
using HeroLineWar.Creatures;
using HeroLineWar.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HeroLineWar.Monsters
{
    public class MonsterShop : MonoBehaviour
    {
        public Button[] mMonsterButtons;
        public GameObject[] mMonsterPrefabs;
        // Start is called before the first frame update
        void Start()
        {
            InitMonsterButtons();
            
        }

        // Update is called once per frame
        void Update()
        {
        }

        void InitMonsterButtons()
        {
            foreach (var button in mMonsterButtons)
            {
                button.onClick.AddListener(OnMonsterButtonClick);
            }
        }

        void BuyMonster(GameObject monsterPrefab)
        {
            GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            Monster monster = monsterPrefab.GetComponent<Monster>();
            if (monster.mCost <= gameManager.mMoney)
            {
                // Envoi du prefab au MonsterManager
                MonsterManager monsterManager = gameManager.mMonsterManager;
                monsterManager.AddPrefabToSpawnList(monsterPrefab);
                // Vol de l'argent
                gameManager.mMoney -= monster.mCost;
            }
        }

        GameObject RetrieveMonsterPrefabByName(string monsterName)
        {
            GameObject res = null;
            foreach (var prefab in mMonsterPrefabs)
            {
                if (prefab.gameObject.name == monsterName)
                {
                    res = prefab;
                }
            }
            return res;
        }

        void OnMonsterButtonClick()
        {
            string buttonName =  EventSystem.current.currentSelectedGameObject.name;
            GameObject retrievedItem = RetrieveMonsterPrefabByName(buttonName);

            if (retrievedItem != null)
            {
                BuyMonster(retrievedItem);
            }
        }
    }
}
