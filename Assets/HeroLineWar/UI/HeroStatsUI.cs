using System;
using HeroLineWar.Creatures;
using HeroLineWar.Items;
using HeroLineWar.Managers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HeroLineWar.UI
{
    public class HeroStatsUI : MonoBehaviour
    {
        #region Variables

        public GameManager mGameManager;
        public Slider mHealthSlider;
        public Image mHealthSliderImage;
        public Text mHealthSliderText;
        public Color mMaxHealthColor = new Color(80f, 255f, 80f);
        public Color mMinHealthColor = new Color(255f, 80f, 80f);
        
        public Slider mManaSlider;
        public Image mManaSliderImage;
        public Text mManaSliderText;
        public Color mManaColor = new Color(115f, 80f, 255f);

        public Slider mXpSlider;
        public Text mXpSliderText;

        public Slider mAbility1Slider;
        public Text mAbility1SliderText;

        public Slider mAbility2Slider;
        public Text mAbility2SliderText;

        public Slider mAbility3Slider;
        public Text mAbility3SliderText;

        public Slider mAbility4Slider;
        public Text mAbility4SliderText;

        public Button[] mItemButtons;


        #endregion
        
        #region Built-in methods
        // Start is called before the first frame update
        void Start()
        {
            mHealthSliderImage.color = mMaxHealthColor;
            mManaSliderImage.color = mManaColor;

            foreach (var button in mItemButtons)
            {
                button.onClick.AddListener(OnItemButtonClick);
                button.enabled = false;
            }
        }

        // Update is called once per frame
        void Update()
        {
            UpdateHealthSlider();
            UpdateManaSlider();
            UpdateXPSlider();
            UpdateAbilitySliders();
            UpdateItemButtons();
        }
        #endregion

        #region Custom methods

        void UpdateManaSlider()
        {
            HeroManager heroManager = mGameManager.mHeroManager;
            Hero hero = heroManager.GetHero();
            float currentMana = hero.mCurrentManaPoints;
            float maxMana = hero.GetMaxManaPoints();

            mManaSlider.value = currentMana / maxMana;
            mManaSliderText.text = $"{currentMana}/{maxMana}";
        }

        void UpdateHealthSlider()
        {
            HeroManager heroManager = mGameManager.mHeroManager;
            Hero hero = heroManager.GetHero();
            float currentHealth = hero.GetCurrentHealthPoints();
            float maxHealth = hero.GetMaxHealthPoints();
            
            mHealthSlider.value = currentHealth / maxHealth;
            mHealthSliderText.text = $"{currentHealth}/{maxHealth}";
            mHealthSliderImage.color = Color.Lerp(mMinHealthColor, mMaxHealthColor, currentHealth / maxHealth);
        }

        void UpdateXPSlider()
        {
            HeroManager heroManager = mGameManager.mHeroManager;
            Hero hero = heroManager.GetHero();
            float currentXP = hero.mExperiencePoints;
            float maxLevelXp = Hero.InitialLevelUpThreshold * hero.mLevel;

            mXpSlider.value = currentXP / maxLevelXp;
            mXpSliderText.text = $"{currentXP}/{maxLevelXp}";
        }

        void UpdateAbilitySliders()
        {
            HeroManager heroManager = mGameManager.mHeroManager;
            Hero hero = heroManager.GetHero();

            if (hero.mAbilities.Count >= 1)
            {
                float ability1CooldownLeft = hero.mAbilities[0].mCooldownEndTimestamp - Time.time;
                float ability1CooldownRatio = ability1CooldownLeft / hero.mAbilities[0].mCooldown;
            
                mAbility1Slider.value = 1 - ability1CooldownRatio;
                if (ability1CooldownLeft > 0f)
                    mAbility1SliderText.text = ability1CooldownLeft.ToString("0.00");
                else
                    mAbility1SliderText.text = "OK";              
            }

            if (hero.mAbilities.Count >= 2)
            {
                float ability2CooldownLeft = hero.mAbilities[1].mCooldownEndTimestamp - Time.time;
                float ability2CooldownRatio = ability2CooldownLeft / hero.mAbilities[1].mCooldown;
                
                mAbility2Slider.value = 1 - ability2CooldownRatio;
                if (ability2CooldownLeft > 0f)
                    mAbility2SliderText.text = ability2CooldownLeft.ToString("0.00");
                else
                    mAbility2SliderText.text = "OK";
            }

            if (hero.mAbilities.Count >= 3)
            {
                float ability3CooldownLeft = hero.mAbilities[2].mCooldownEndTimestamp - Time.time;
                float ability3CooldownRatio = ability3CooldownLeft / hero.mAbilities[2].mCooldown;
                
                mAbility3Slider.value = 1 - ability3CooldownRatio;
                if (ability3CooldownLeft > 0f)
                    mAbility3SliderText.text = ability3CooldownLeft.ToString("0.00");
                else
                    mAbility3SliderText.text = "OK";
            }

            if (hero.mAbilities.Count >= 4)
            {
                float ability4CooldownLeft = hero.mAbilities[3].mCooldownEndTimestamp - Time.time;
                float ability4CooldownRatio = ability4CooldownLeft / hero.mAbilities[3].mCooldown;
                
                mAbility4Slider.value = 1 - ability4CooldownRatio;
                if (ability4CooldownLeft > 0f)
                    mAbility4SliderText.text = ability4CooldownLeft.ToString("0.00");
                else
                    mAbility4SliderText.text = "OK";
            }
            
        }

        void UpdateItemButtons()
        {
            Hero hero = mGameManager.mHeroManager.GetHero();
            for (int buttonIdx = 0; buttonIdx < mItemButtons.Length; buttonIdx++)
            {
                Button currentButton = mItemButtons[buttonIdx];
                if (buttonIdx < hero.mItemList.Count)
                {
                    Item currentItem = hero.mItemList[buttonIdx];
                    currentButton.GetComponentInChildren<Text>().text = currentItem.mName;
                    currentButton.enabled = true;
                }
                else
                {
                    currentButton.GetComponentInChildren<Text>().text = "";
                    currentButton.enabled = false;
                }
            }
        }

        void OnItemButtonClick()
        {
            Hero hero = mGameManager.mHeroManager.GetHero();
            string buttonName =  EventSystem.current.currentSelectedGameObject.name;
            int buttonIdx = int.Parse(buttonName.Split('_')[1])-1;
            Item usedItem = hero.mItemList[buttonIdx];

            if (usedItem is Consumable usedConsumable)
            {
                usedConsumable.Consume();
            }
            hero.mItemList.RemoveAt(buttonIdx);
        }

        #endregion
    }
}
