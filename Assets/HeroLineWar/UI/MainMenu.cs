using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Dropdown mHeroDropdown;
    public Button mStartButton;
    public Button mExitButton;
    
    // Start is called before the first frame update
    void Start()
    {
        mStartButton.onClick.AddListener(OnStartButtonClick);
        mExitButton.onClick.AddListener(OnExitButtonClick);
    }   

    void OnStartButtonClick()
    {
        PlayerPrefs.SetString("HeroName", mHeroDropdown.options[mHeroDropdown.value].text);
        SceneManager.LoadScene(1);
    }

    void OnExitButtonClick()
    {
        Application.Quit();
    }
}
