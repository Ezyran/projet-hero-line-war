using HeroLineWar.Managers;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HeroLineWar.UI
{
    public class GameStatsUI : MonoBehaviour
    {
        #region Variables

        public GameManager mGameManager;
        public Slider mNexusHealthSlider;
        public Image mNexusHealthSliderImage;
        public Text mNexusHealthSliderText;
        private Color _mMaxHealthColor = new Color(0.31f, 1f, 0.31f, 1f);
        private Color _mMinHealthColor = new Color(1f, 0.31f, 0.31f, 1f);

        public Text mMoneyText;
        
        public Button mShopButton;
        public Canvas mShopCanvas;
        
        public Button mMonstersButton;
        public Canvas mMonstersCanvas;

        public Button mQuitButton;
        
        #endregion
        
        #region Built-in methods
        
        // Start is called before the first frame update
        void Start()
        {
            mShopButton.onClick.AddListener(OnShopButtonClick);
            mMonstersButton.onClick.AddListener(OnMonsterButtonClick);
            mQuitButton.onClick.AddListener(OnQuitButtonClick);

            mShopCanvas.enabled = false;
            mMonstersCanvas.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            UpdateNexusHealth();
            UpdateMoney();

            if (Input.GetAxis("Cancel") > 0)
            {
                DisableMenusCanvas();
            }
            
        }
        
        #endregion

        #region Custom methods

        void UpdateNexusHealth()
        {
            float currentHealth = mGameManager.mNexus.GetCurrentHealthPoints();
            float maxHealth = mGameManager.mNexus.GetMaxHealthPoints();

            mNexusHealthSlider.value = currentHealth / maxHealth;
            mNexusHealthSliderImage.color = Color.Lerp(_mMinHealthColor, _mMaxHealthColor, currentHealth / maxHealth);
            mNexusHealthSliderText.text = $"{currentHealth}/{maxHealth}";
        }

        void UpdateMoney()
        {
            mMoneyText.text = mGameManager.mMoney.ToString();
        }

        void DisableMenusCanvas()
        {
            mShopCanvas.enabled = false;
            mMonstersCanvas.enabled = false;
        }

        void OnShopButtonClick()
        {
            mMonstersCanvas.enabled = false;
            if (!mShopCanvas.enabled)
                mShopCanvas.enabled = true;
            else
                mShopCanvas.enabled = false;
        }

        void OnMonsterButtonClick()
        {
            mShopCanvas.enabled = false;
            if (!mMonstersCanvas.enabled)
                mMonstersCanvas.enabled = true;
            else
                mMonstersCanvas.enabled = false;
        }

        void OnQuitButtonClick()
        {
            SceneManager.LoadScene(0);
        }

        #endregion

    }
}
