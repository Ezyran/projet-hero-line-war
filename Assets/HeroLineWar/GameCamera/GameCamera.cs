using UnityEngine;

namespace HeroLineWar.GameCamera
{
    public class GameCamera : MonoBehaviour
    {
        #region Variables

        [Header("Movement properties")] 
        public float mSpeed = 0.5f;
        public float mZoomSpeed = 10f;

        [Header("Zoom properties")] 
        public float mMaxHeight = 10f;
        public float mMinHeight = 5f;
        public float mMaxPosX = 30f;
        public float mMinPosX = -30f;
        public float mMaxPosZ = 5f;
        public float mMinPosZ = -20f;
        
        #endregion

        // Update is called once per frame
        void Update()
        {
            UpdateCameraPosition();
        }

        void UpdateCameraPosition()
        {
            float horizontalSpeed = mSpeed * Input.GetAxis("Horizontal");
            float verticalSpeed = mSpeed * Input.GetAxis("Vertical");
            float scrollSpeed = -mZoomSpeed * Input.GetAxis("Mouse ScrollWheel");

            // Contrôle du résultat du zoom pour ne pas dépasser les limites de hauteur
            float nextHeight = transform.position.y + scrollSpeed;
            if (nextHeight >= mMaxHeight)
            {
                scrollSpeed = mMaxHeight - transform.position.y;
            } 
            else if (nextHeight < mMinHeight)
            {
                scrollSpeed = transform.position.y - mMinHeight;
            }
            
            Vector3 zoomMove = new Vector3(0, scrollSpeed, 0);
            Vector3 horizontalMove = horizontalSpeed * transform.right;
            Vector3 verticalMove = transform.forward;
            verticalMove.y = 0;
            verticalMove.Normalize();
            verticalMove *= verticalSpeed;

            Vector3 cameraMove = zoomMove + horizontalMove + verticalMove;
            // On vérifie si la caméra ne va pas sortir de ses limites
            Vector3 nextPos = transform.position + cameraMove;
            
            if (nextPos.x < mMinPosX || nextPos.x > mMaxPosX)
                cameraMove.x = 0f;
            if (nextPos.z < mMinPosZ || nextPos.z > mMaxPosZ)
                cameraMove.z = 0f;

            transform.position += cameraMove;
        }
    }
}
